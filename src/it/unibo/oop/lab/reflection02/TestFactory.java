package it.unibo.oop.lab.reflection02;

import org.junit.Test;

/**
 * Act as a convenience JUnit test of a robot factory.
 * 
 */
public class TestFactory {

    /**
     * Test a Robot Factory.
     */
    @Test
    public void testRobotFactory() {
        /*
         * 1) Creare tre diverse istanze di robot (una per ogni tipo) tramite
         * ComposableRobotFactory
         * 
         * 2) effettuare alcuni test a piacere volti a verificare il corretto
         * funzionamento dei metodi del robot: si usino asserzioni JUnit
         * opportune.
         */
    }
}