package it.unibo.oop.lab.reflection02;

/**
 * 
 * A generic robotic exception.
 *
 */
@SuppressWarnings("serial")
public class RobotException extends Exception {

}