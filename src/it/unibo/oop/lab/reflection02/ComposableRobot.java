package it.unibo.oop.lab.reflection02;

/**
 * Models a generic composable robot.
 * 
 */
public interface ComposableRobot {
    /**
     * 
     * @return The composable robot's parts.
     */
    RobotPart[] getParts();
}